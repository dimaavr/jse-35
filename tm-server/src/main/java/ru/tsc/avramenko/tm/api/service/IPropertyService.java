package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService extends ISaltSettings, ISessionSettings {

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    Integer getBackupInterval();

}