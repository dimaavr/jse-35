package ru.tsc.avramenko.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.tsc.avramenko.tm.api.repository.ISessionRepository;
import ru.tsc.avramenko.tm.model.Session;

public class SessionRepository extends AbstractOwnerRepository<Session> implements ISessionRepository {

    @Override
    public boolean contains(@NotNull final String id) {
        return list.stream().anyMatch(e -> id.equals(e.getId()));
    }

}