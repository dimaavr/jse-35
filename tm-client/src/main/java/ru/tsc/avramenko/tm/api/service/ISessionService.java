package ru.tsc.avramenko.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.tsc.avramenko.tm.endpoint.Session;

public interface ISessionService {

    @Nullable
    Session getSession();

    void setSession(@Nullable Session session);

}