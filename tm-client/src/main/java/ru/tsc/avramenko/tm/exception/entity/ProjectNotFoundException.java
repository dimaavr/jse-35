package ru.tsc.avramenko.tm.exception.entity;

import ru.tsc.avramenko.tm.exception.AbstractException;

public class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error! Project not found.");
    }

}